unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Datasnap.DBClient,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, uDBGridBR, Vcl.StdCtrls, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    DBGridBR1: TDBGridBR;
    Panel1: TPanel;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    ClientDataSet1ID: TIntegerField;
    ClientDataSet1NOME: TStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    CheckBox1: TCheckBox;
    ColorListBox1: TColorListBox;
    ColorListBox2: TColorListBox;
    Label1: TLabel;
    Cor2: TLabel;
    TabSheet2: TTabSheet;
    CheckBox2: TCheckBox;
    Label2: TLabel;
    ColorListBox3: TColorListBox;
    TabSheet3: TTabSheet;
    CheckBox3: TCheckBox;
    Label3: TLabel;
    ColorListBox4: TColorListBox;
    Filtrar: TTabSheet;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    TabSheet4: TTabSheet;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ColorListBox1Click(Sender: TObject);
    procedure ColorListBox2Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure ColorListBox3Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure ColorListBox4Click(Sender: TObject);
    procedure CheckBox4Click(Sender: TObject);
    procedure CheckBox5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure CarregarCDS;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.CheckBox1Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.Zebrar := CheckBox1.Checked;
  DBGridBR1.Repaint;
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.DestacarCelula := CheckBox2.Checked;
  DBGridBR1.Repaint;
end;

procedure TForm1.CheckBox3Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.DestacarLinha := CheckBox3.Checked;
  DBGridBR1.Repaint;
end;

procedure TForm1.CheckBox4Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.Filtrar := CheckBox4.Checked;
end;

procedure TForm1.CheckBox5Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.Localizar := CheckBox5.Checked;
end;

procedure TForm1.ColorListBox1Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.ZebrarCor1 := ColorListBox1.Selected;
  DBGridBR1.Repaint;
end;

procedure TForm1.ColorListBox2Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.ZebrarCor2 := ColorListBox2.Selected;
  DBGridBR1.Repaint;
end;

procedure TForm1.ColorListBox3Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.DestacarCelulaCor := ColorListBox3.Selected;
  DBGridBR1.Repaint;
end;

procedure TForm1.ColorListBox4Click(Sender: TObject);
begin
  DBGridBR1.ConfigBR.DestacarLinhaCor := ColorListBox4.Selected;
  DBGridBR1.Repaint;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ClientDataSet1.CreateDataSet;
  CarregarCDS;

  ColorListBox1.Selected := DBGridBR1.ConfigBR.ZebrarCor1;
  ColorListBox2.Selected := DBGridBR1.ConfigBR.ZebrarCor2;
  ColorListBox3.Selected := DBGridBR1.ConfigBR.DestacarCelulaCor;
  ColorListBox4.Selected := DBGridBR1.ConfigBR.DestacarLinhaCor;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  DBGridBR1.ExportarCSV;
end;

procedure TForm1.CarregarCDS;
  procedure Add(AID: integer; ANome: string);
  begin
    ClientDataSet1.Insert;
    ClientDataSet1ID.AsInteger := AID;
    ClientDataSet1NOME.AsString := ANome;
    ClientDataSet1.Post;

  end;
begin
  ClientDataSet1.Close;
  ClientDataSet1.Open;
  ClientDataSet1.EmptyDataSet;

  Add(1, 'JOAO');
  Add(2, 'MARIA');
  Add(3, 'PEDRO');
  Add(4, 'JOANA');
  Add(5, 'JOSE');
end;

end.
