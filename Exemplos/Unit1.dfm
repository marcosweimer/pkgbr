object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 449
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridBR1: TDBGridBR
    Left = 0
    Top = 0
    Width = 527
    Height = 256
    Align = alClient
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    ConfigBR.Zebrar = True
    ConfigBR.ZebrarCor1 = clSilver
    ConfigBR.ZebrarCor2 = clMoneyGreen
    ConfigBR.DestacarCelula = False
    ConfigBR.DestacarCelulaCor = clRed
    ConfigBR.DestacarLinha = False
    ConfigBR.DestacarLinhaCor = clYellow
    ConfigBR.Filtrar = True
    ConfigBR.Localizar = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 256
    Width = 527
    Height = 193
    Align = alBottom
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 525
      Height = 191
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Zebrar'
        object Label1: TLabel
          Left = 5
          Top = 26
          Width = 23
          Height = 13
          Caption = 'Cor1'
        end
        object Cor2: TLabel
          Left = 142
          Top = 26
          Width = 23
          Height = 13
          Caption = 'Cor2'
        end
        object CheckBox1: TCheckBox
          Left = 3
          Top = 3
          Width = 65
          Height = 17
          Caption = 'Zebrar'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CheckBox1Click
        end
        object ColorListBox1: TColorListBox
          Left = 5
          Top = 45
          Width = 121
          Height = 97
          TabOrder = 1
          OnClick = ColorListBox1Click
        end
        object ColorListBox2: TColorListBox
          Left = 142
          Top = 45
          Width = 121
          Height = 97
          TabOrder = 2
          OnClick = ColorListBox2Click
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Destacar Celula'
        ImageIndex = 1
        object Label2: TLabel
          Left = 16
          Top = 26
          Width = 21
          Height = 13
          Caption = 'Cor:'
        end
        object CheckBox2: TCheckBox
          Left = 16
          Top = 3
          Width = 97
          Height = 17
          Caption = 'Destacar'
          TabOrder = 0
          OnClick = CheckBox2Click
        end
        object ColorListBox3: TColorListBox
          Left = 15
          Top = 44
          Width = 121
          Height = 97
          TabOrder = 1
          OnClick = ColorListBox3Click
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Destacar Linha'
        ImageIndex = 2
        object Label3: TLabel
          Left = 24
          Top = 34
          Width = 21
          Height = 13
          Caption = 'Cor:'
        end
        object CheckBox3: TCheckBox
          Left = 24
          Top = 11
          Width = 97
          Height = 17
          Caption = 'Destacar'
          TabOrder = 0
          OnClick = CheckBox3Click
        end
        object ColorListBox4: TColorListBox
          Left = 23
          Top = 52
          Width = 121
          Height = 97
          TabOrder = 1
          OnClick = ColorListBox4Click
        end
      end
      object Filtrar: TTabSheet
        Caption = 'Filtrar/Localizar'
        ImageIndex = 3
        object CheckBox4: TCheckBox
          Left = 16
          Top = 15
          Width = 329
          Height = 17
          Caption = 
            'Ativar Filtro (aceita F3 na grid para filtrar pela coluna com fo' +
            'co)'
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CheckBox4Click
        end
        object CheckBox5: TCheckBox
          Left = 16
          Top = 38
          Width = 368
          Height = 17
          Caption = 
            'Ativar Localizar (aceita F4 na grid para localizar pela coluna c' +
            'om foco)'
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = CheckBox5Click
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Exportar'
        ImageIndex = 4
        object Button1: TButton
          Left = 16
          Top = 3
          Width = 75
          Height = 25
          Caption = 'CSV'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 216
    Top = 72
    object ClientDataSet1ID: TIntegerField
      FieldName = 'ID'
    end
    object ClientDataSet1NOME: TStringField
      FieldName = 'NOME'
    end
  end
  object DataSource1: TDataSource
    DataSet = ClientDataSet1
    Left = 208
    Top = 128
  end
end
